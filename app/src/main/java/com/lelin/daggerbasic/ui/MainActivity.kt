package com.lelin.daggerbasic.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.lelin.daggerbasic.model.BangladeshCricketTeam
import com.lelin.daggerbasic.model.Players
import com.lelin.daggerbasic.R
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject lateinit var team: BangladeshCricketTeam
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_show.setOnClickListener {
            textView.setText(team.getBestEleven())
            textViews.setText(team.getCoachName())
        }

    }
}