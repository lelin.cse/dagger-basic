package com.lelin.daggerbasic.di.module

import com.lelin.daggerbasic.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector
    abstract fun bindMainActivity():MainActivity
}