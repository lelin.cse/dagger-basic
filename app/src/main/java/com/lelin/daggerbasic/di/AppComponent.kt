package com.lelin.daggerbasic.di

import com.lelin.daggerbasic.App
import com.lelin.daggerbasic.di.module.ActivityBuilderModule
import com.lelin.daggerbasic.di.module.CoachModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuilderModule::class,
        CoachModule::class
    ]
)
interface AppComponent:AndroidInjector<App>