package com.lelin.daggerbasic.di.module

import com.lelin.daggerbasic.model.CoachRussel
import dagger.Module
import dagger.Provides

@Module
class CoachModule {
    @Provides
    fun provideCoach():CoachRussel= CoachRussel.getCoachRusselInstance()
}