package com.lelin.daggerbasic.model

class CoachRussel private constructor(){

    companion object{
        fun getCoachRusselInstance():CoachRussel= CoachRussel()

    }
    fun getName():String ="Russel Domingo"

    fun getSpecility():String ="Batting & Young Player"
}