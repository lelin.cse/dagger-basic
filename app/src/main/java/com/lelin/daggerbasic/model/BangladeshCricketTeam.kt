package com.lelin.daggerbasic.model

import javax.inject.Inject

class BangladeshCricketTeam @Inject constructor(val players: Players,val coach:CoachRussel) {

    fun getBestEleven():String=players.getPlayersName()
    fun getCoachName():String=coach.getName()
}